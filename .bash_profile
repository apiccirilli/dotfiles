alias ls="ls -gArtGph"
alias vi=vim
alias matlab='/Applications/MATLAB_R2017a.app/bin/matlab -nosplash -nodisplay'

export PATH=$PATH:/Volumes/ToolsMac/bin

PS1="\u@ostrich:\W $ "

if [ -f ~/.bashrc ]; then
   source ~/.bashrc
fi

# FSL Setup
FSLDIR=/usr/local/fsl
PATH=${FSLDIR}/bin:${PATH}
export FSLDIR PATH
. ${FSLDIR}/etc/fslconf/fsl.sh

clr() {
    clear
    cd -P ${1:-~/workspace}
    ls -lArt
    echo -e "\n`pwd`\n"
}

export PATH="$HOME/.cargo/bin:$PATH"
