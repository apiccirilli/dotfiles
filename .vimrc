set nocompatible
set hidden

call plug#begin('~/.vim/plugged')

Plug 'w0rp/ale'
Plug 'christoomey/vim-tmux-navigator'
Plug 'altercation/vim-colors-solarized'
Plug 'NLKNguyen/papercolor-theme'
Plug 'reedes/vim-colors-pencil'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'dhruvasagar/vim-table-mode'
Plug 'plasticboy/vim-markdown'
Plug 'chrisbra/csv.vim'
Plug 'scrooloose/nerdtree'

call plug#end()

" Specialized syntax/indent/plugin per filetype
syntax enable
filetype plugin indent on
set omnifunc=syntaxcomplete#Complete

" Tab in insert mode or >> << are 4 spaces; hard tabs take up 8 columns
set expandtab
set shiftwidth=4
set softtabstop=4
set tabstop=8

" Show relative line numbers on the side except for the current line
set relativenumber
set number

" Only do case-sensitive search if I include capitals
set ignorecase
set smartcase

" Highlight current line and the 80th column
set cursorline
set colorcolumn=80

" New splits go down or to the right by default
set splitbelow
set splitright

" Easier split navigation
nnoremap <C-j> <C-W><C-j>
nnoremap <C-k> <C-W><C-k>
nnoremap <C-l> <C-W><C-l>
nnoremap <C-h> <C-W><C-h>

" Leader
nmap <space> <leader>
nnoremap <leader>\| <C-w><C-v><C-w><C-l>
nnoremap <leader>- <C-w><C-s><C-w><C-j>
nnoremap <leader>w :w<CR>
nnoremap <leader>wq :wqa<CR>
nnoremap <leader>q :qa<CR>
nnoremap <leader>n :bn<CR>
nnoremap <leader>mt :Toch<CR>
nnoremap <leader>f :NERDTree<CR><C-w>=

" Mouse
set mouse=a

" Color scheme solarized
set t_Co=256
set background=light
colorscheme PaperColor

" Use whole 'words' when opening URLs.
" This avoids cutting off parameters (after '?') and anchors (after '#').
" See http://vi.stackexchange.com/q/2801/1631
let g:netrw_gx="<cWORD>"

" Plugin settings
" Ale
let g:ale_set_loclist = 0

" Airline
let g:airline#extensions#tabline#enabled = 1 " show buffers on top
let g:airline#extensions#tabline#switch_buffers_and_tabs = 0
let g:airline#extensions#tabline#fnamemod = ':t'
let g:airline#extensions#csv#column_display = 'Name'

" plasticboy/vim-markdown
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_math = 1
let g:vim_markdown_new_list_item_indent = 0
au BufRead,BufNewFile *.md setlocal conceallevel=2 sw=2 sts=2 tw=80
:map <leader>tc :Toch<CR>

let g:table_mode_corner='|'
let g:table_mode_align_char=':'
